# sentinel-npm
*An NPM executable package for HashiCorp's Sentinel.*

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier) [![sentinel: v0.11.7](https://img.shields.io/badge/sentinel-v0.11.7.0-6253f4.svg)](https://www.sentinel.io) [![npm downloads](https://img.shields.io/npm/dt/sentinel-npm.svg?maxAge=3600)](https://www.npmjs.com/package/sentinel-npm) [![Travis CI](https://img.shields.io/travis/steven-xie/sentinel-npm.svg)](https://travis-ci.org/steven-xie/sentinel-npm) [![CodeClimate: Maintainability](https://api.codeclimate.com/v1/badges/a1b6ec5bf81af570f5bc/maintainability)](https://codeclimate.com/github/steven-xie/sentinel-npm/maintainability)

### Preamble
I assembled [Sentinel](https://sentinel.io) into an NPM package in order for me to include it in other projects that depended on the executable. I wanted to be able to publish NPM modules with scripts like this:
```json
{
  "scripts": {
    "plan": "sentinel plan -out=my-tfplan"
  }
}
```
But without having to worry about asking users to download Sentinel externally.

---

### Installation
To use *Sentinel* as an NPM package, include it in your `package.json` dependencies:
```bash
# If you're using Yarn (recommended):
yarn add sentinel-npm

# If you're using NPM:
npm i sentinel-npm
```

Or, if you want a one-time installation that you can run arbitrarily, install it globally:
```bash
# If you're using Yarn (recommended):
yarn global add sentinel-npm

# If you're using NPM:
npm i -g sentinel-npm
```


## Usage
#### As a project dependency:
This package cannot currently be used as a typical Node module, as it does not export any entry points; it only symlinks a binary. So, the recommended use case is to use it in your `package.json` scripts:
```json
{
    "scripts": {
        "plan": "sentinel plan -out=my-tfplan",
        "apply": "sentinel apply",
        "execute": "sentinel apply \"my-tfplan\"",
        "destroy": "sentinel destroy"
    }
}
```

#### As a globally installed binary:
If you installed this package globally (with `npm i -g` or `yarn global add`), you can simply start using it like a regular command-line program:
```bash
sentinel -v        # show version info
sentinel --help    # show usage info
```
