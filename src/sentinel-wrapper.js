#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

const execName = process.platform === 'win32' ? 'sentinel.exe' : './sentinel';
const command = resolve(__dirname, '..', 'tools', execName);
const sentinel = spawn(command, process.argv.slice(2), { cwd: process.cwd() });

sentinel.stdout.pipe(process.stdout);
sentinel.stderr.pipe(process.stderr);
sentinel.on('error', function(err) {
  console.error(`Received an error while executing the Sentinel binary: ${err}`);
  process.exit(1);
});
